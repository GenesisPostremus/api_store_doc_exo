from django.db import models


class Produit(models.Model):
    libelle = models.CharField(max_length=30)
    prix = models.IntegerField(max_length=5)

    def __str__(self):
        return self.libelle
