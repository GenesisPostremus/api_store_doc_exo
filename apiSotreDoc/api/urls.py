from django.urls import path

from api import views

urlpatterns = [
    path('', views.ProduitList.as_view()),
    path('/<int:pk>/', views.ProduitDetail.as_view())
]