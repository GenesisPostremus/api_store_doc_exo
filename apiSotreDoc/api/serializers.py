from rest_framework import serializers

from api.models import Produit


class ProduitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Produit
        fields = ('libelle', 'prix')
